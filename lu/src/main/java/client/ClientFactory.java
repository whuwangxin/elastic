package client;

import config.ConfigLoad;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

/**
 * @Author unclewang
 * @Date 2018/8/27 14:46
 */
public class ClientFactory {
    public volatile static TransportClient client;

    public static TransportClient getClient() {

        //ignore_cluster_name 如果集群名不对，也能连接
        //name 连接的集群名
        Settings settings = Settings.builder()
                .put("cluster.name", ConfigLoad.getClusterName())
                .put("client.transport.sniff", true)
                .put("client.transport.ignore_cluster_name", true)
                .build();
        //创建client
        //主机和端口号
        if (client == null) {
            synchronized (ClientFactory.class) {
                client = new PreBuiltTransportClient(settings)
                        .addTransportAddress(ConfigLoad.getUrl());
            }
        }

        return client;
    }
}
