package query;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.List;

/**
 * @author unclewang
 */
public class IdSearch extends Search {

    @Override
    public List<SearchHit> query(String id) {
        index = "aminer";
        type = "aminer";
        queryBuilder = QueryBuilders.matchPhraseQuery("id", id);
        fetchSource = new String[]{"id", "venue"};
        SearchResponse searchResponse = Search.createResponse(index, type, queryBuilder, fetchSource);
        List<SearchHit> result = query(searchResponse);
        return result;
    }
}
