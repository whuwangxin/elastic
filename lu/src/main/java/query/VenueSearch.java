package query;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.List;

/**
 * @Author unclewang
 * @Date 2018/8/27 14:58
 */
public class VenueSearch extends Search {

    @Override
    public List<SearchHit> query(String journal) {
        index = "aminer";
        type = "aminer";
        queryBuilder = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchPhraseQuery("venue", journal))
                .must(QueryBuilders.existsQuery("references"));
//                .must(QueryBuilders.matchPhraseQuery("year", "2012"));
        fetchSource = new String[]{"id", "title", "venue", "references"};
        SearchResponse searchResponse = Search.createResponse(index, type, queryBuilder, fetchSource);
        List<SearchHit> result = query(searchResponse);
        System.out.println(result.size());
        return result;
    }

    public static void main(String[] args) {
        VenueSearch vq = new VenueSearch();
        vq.query("MIS Quarterly");
    }
}
