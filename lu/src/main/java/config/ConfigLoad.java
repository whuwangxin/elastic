package config;

import org.elasticsearch.common.transport.TransportAddress;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ConfigLoad {
    private static final String CONFIGFILE = "elastic53.properties";
    private static final String ClusterName = "es.cluster.name";
    private static final String TransportIPPort = "es.transport.address";
    private static final String NumOfShards = "es.num.shards";
    private static final String NumOfReplicas = "es.num.replicas";
    private static final String BulkActions = "es.bulk.actions";
    private static final String BulkSize = "es.bulk.size";
    private static final String JournalList = "journal.list.filepath";
    private static final String Index = "es.index";
    private static final String Type = "es.type";

    private static Properties properties = new Properties();

    static {
        InputStream in;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(CONFIGFILE);
            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static TransportAddress getUrl() {
        String[] ta = getTransportAddress().split(":");
        TransportAddress transportAddress = null;
        try {
            transportAddress = new TransportAddress(InetAddress.getByName(ta[0]), Integer.parseInt(ta[1]));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return transportAddress;
    }

    public static String getClusterName() {
        return properties.getProperty(ClusterName);
    }

    public static String getTransportAddress() {
        return properties.getProperty(TransportIPPort, "127.0.0.1:9300");
    }

    public static Integer getNumOfShards() {
        return Integer.valueOf(properties.getProperty(NumOfShards));
    }

    public static Integer getNumOfReplicas() {
        return Integer.valueOf(properties.getProperty(NumOfReplicas));
    }

    public static Integer getBulkActions() {
        return Integer.valueOf(properties.getProperty(BulkActions));
    }

    public static Integer getBulkSize() {
        return Integer.valueOf(properties.getProperty(BulkSize));
    }

    public static String getIndex() {
        return properties.getProperty(Index);
    }

    public static String getType() {
        return properties.getProperty(Type);
    }

    public static List<String> getJournalList() {
        File file = new File(properties.getProperty(JournalList));
        List<String> journalList = new ArrayList<>();
        try {
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
                line = line.replaceAll("&amp;", "&").toUpperCase();
                journalList.add(line);
            }
            br.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println(journalList.toString());
        return journalList;
    }

    public static void main(String[] args) {
        getJournalList();
    }
}
