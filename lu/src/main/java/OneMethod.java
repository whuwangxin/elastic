import config.ConfigLoad;
import org.elasticsearch.search.SearchHit;
import query.IdSearch;
import query.VenueSearch;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OneMethod {
    private static VenueSearch vs = new VenueSearch();
    private static IdSearch is = new IdSearch();
    private static HashMap<String, Integer> resultMap = new HashMap<>();

    public static void main(String[] args) {
        List<String> journalList = ConfigLoad.getJournalList();
        Map passageMap;
        Map referenceMap;
        System.out.println("共有期刊数量：" + journalList.size());
        int journalId = 0;

        for (String journal : journalList) {
            journalId++;

            System.out.println("Journal" + journalId + " 正在遍历期刊:" + journal);
            List<SearchHit> passageList = vs.query(journal);
            System.out.println("共有文章数量：" + passageList.size());
            int passageId = 0;
            for (SearchHit hit : passageList) {
                passageMap = hit.getSourceAsMap();
                String title = (String) passageMap.get("title");
                passageId++;
                System.out.println("Journal" + journalId + ";Passage" + passageId + " 正在遍历文章:" + title);
                List<String> references = (List<String>) passageMap.get("references");
                System.out.println("共有引用数量：" + references.size());
                int referenceId = 0;
                for (String reference : references) {
                    referenceId++;
                    List<SearchHit> searchHits = is.query(reference);
                    if (searchHits.size() == 0) {
                        continue;
                    }
                    SearchHit searchHit = searchHits.get(0);
                    referenceMap = searchHit.getSourceAsMap();
                    String referenceJournal = (String) referenceMap.get("venue");
                    if (referenceJournal==null){
                        continue;
                    }
                    referenceJournal = referenceJournal.replaceAll("J\\.", "Journal").toUpperCase();

                    //System.out.println(referenceId + " " + referenceJournal);
                    if (journalList.contains(referenceJournal)) {
                        String key = concatString(journal, referenceJournal);
                        if (!resultMap.containsKey(key)) {
                            resultMap.put(key, 0);
                        }
                        resultMap.put(key, resultMap.get(key) + 1);
                    }
                }
                System.out.printf("现在hashmap的size"+resultMap.size());
            }
        }
        try {
            ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(new File("hashmap.save")));
            oos.writeObject(resultMap);
            oos.close();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("hashmap.save")));
            HashMap<String,Integer> stringIntegerHashMap= (HashMap<String, Integer>) ois.readObject();
            System.out.println(stringIntegerHashMap.size());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
//        System.out.println(resultMap.toString());
    }

    public static String concatString(String a, String b) {
        String result;
        if (a.compareTo(b) < 0) {
            result = a + "||" + b;
        } else {
            result = b + "||" + a;
        }
        return result;
    }
}
