package elastic.learn.transport.search;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

public class Aggregations {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        SearchResponse sr = client.prepareSearch().setIndices("index")
                .setQuery(QueryBuilders.matchAllQuery())
                .addAggregation(
                        AggregationBuilders.terms("agg1").field("gender.keyword")
                )
                .addAggregation(
                        AggregationBuilders.dateHistogram("agg2")
                                .field("age")
                                .dateHistogramInterval(DateHistogramInterval.HOUR)
                )
                .get();

        // Get your facet results
        Terms agg1 = sr.getAggregations().get("agg1");
        Histogram agg2 = sr.getAggregations().get("agg2");

        System.out.println(agg1.toString());
        System.out.println(agg2.toString());
    }
}
