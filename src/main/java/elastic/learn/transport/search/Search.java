package elastic.learn.transport.search;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;

public class Search {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        SearchResponse response = client.prepareSearch("index")
                .setTypes("type")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.termQuery("gender", "male"))                 // Query
                .setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(18))     // Filter
                .setFrom(0)
                .setSize(60)
                .setExplain(true)
                .get();
        SearchResponse responseAll = client.prepareSearch().get();
        System.out.println(response);
        System.out.println(responseAll);
    }
}
