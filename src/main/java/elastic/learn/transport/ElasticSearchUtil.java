package elastic.learn.transport;

import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;


public class ElasticSearchUtil {

    private static TransportClient client;

    public ElasticSearchUtil() {
        client = ElasticsearchConfig.getElasticsearchClient();  //使用上面创建好的客户端添加到类中。
    }

    //创建索引，并给索引某些字段指定iK分词，以后向该索引中查询时，就会用ik分词。
    public void createIndex() throws IOException {
        //创建映射
        //      .startObject("m_id").field("type","keyword").endObject()
        //title:字段名，  type:文本类型       analyzer ：分词器类型
        XContentBuilder mapping = XContentFactory.jsonBuilder()
                .startObject()
                .startObject("properties")
                .startObject("title").field("type", "text").field("analyzer", "ik_smart").endObject()   //该字段添加的内容，查询时将会使用ik_smart分词
                .startObject("content").field("type", "text").field("analyzer", "ik_max_word").endObject()
                .endObject()
                .endObject();

        //index：索引名   type：类型名（可以自己定义）
        PutMappingRequest putmap = Requests.putMappingRequest("index").type("type").source(mapping);
        //创建索引
        client.admin().indices().prepareCreate("index").execute().actionGet();
        //为索引添加映射
        client.admin().indices().putMapping(putmap).actionGet();
    }
}