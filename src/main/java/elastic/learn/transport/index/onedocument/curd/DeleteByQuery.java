package elastic.learn.transport.index.onedocument.curd;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;

public class DeleteByQuery {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();


        //要等一下才能运行成功，可以使用等待
        DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                .filter(QueryBuilders.matchQuery("age", 25))
                .source("twitter")
                .execute(new ActionListener<BulkByScrollResponse>() {
                    @Override
                    public void onResponse(BulkByScrollResponse bulkByScrollResponse) {
                        System.err.println(bulkByScrollResponse.getDeleted());
                    }

                    @Override
                    public void onFailure(Exception e) {
                        System.err.println("没有删除");
                    }
                });


        BulkByScrollResponse response = DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
                .filter(QueryBuilders.matchQuery("age", 25))
                .source("twitter")
                .get();
        long deleted = response.getDeleted();
        System.out.println(deleted);


    }

}
