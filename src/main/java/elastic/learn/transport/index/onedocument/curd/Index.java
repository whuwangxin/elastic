package elastic.learn.transport.index.onedocument.curd;


import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import elastic.learn.transport.ElasticsearchConfig;
import elastic.learn.transport.index.onedocument.Friend;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.*;

public class Index {
    public static void main(String[] args) {
        index();
    }

    public static void index() {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        List<IndexResponse> responses = new ArrayList<>();

        //json
        String json = "{" +
                "\"user\":\"kimchy\"," +
                "\"postDate\":\"2013-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";

        IndexResponse responseJson = client.prepareIndex("twitter", "tweet", "1")
                .setSource(json, XContentType.JSON)
                .get();
        responses.add(responseJson);

        //map
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("user", "kimchy");
        jsonMap.put("postDate", new Date());
        jsonMap.put("message", "trying out Elasticsearch");
        IndexResponse responseMap = client.prepareIndex("twitter", "tweet", "2")
                .setSource(jsonMap)
                .get();
        responses.add(responseMap);

        //jackson
        Friend friend = new Friend("yitian", 25);

        try {
            ObjectMapper mapper = new ObjectMapper();
            byte[] jackson = mapper.writeValueAsBytes(friend);
            IndexResponse responseByte = client.prepareIndex("twitter", "tweet", "3")
                    .setSource(jackson, XContentType.JSON)
                    .get();
            responses.add(responseByte);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        //fastjson
        String fastJson = JSON.toJSONString(friend);
        IndexResponse responseFast = client.prepareIndex("twitter", "tweet", "5")
                .setSource(fastJson, XContentType.JSON)
                .get();
        responses.add(responseFast);
        String gson = new Gson().toJson(friend);
        IndexResponse responseGson = client.prepareIndex("twitter", "tweet", "6")
                .setSource(gson, XContentType.JSON)
                .get();
        responses.add(responseGson);
        //es
        XContentBuilder builder = null;
        try {
            builder = XContentFactory.jsonBuilder()
                    .startObject()
                    .field("user", "kimchy")
                    .field("postDate", new Date())
                    .field("message", "trying out Elasticsearch")
                    .endObject();

        } catch (IOException e) {
            e.printStackTrace();
        }

        IndexResponse responseXcontent = client.prepareIndex("twitter", "tweet", "4")
                .setSource(builder, XContentType.JSON)
                .get();
        responses.add(responseXcontent);
        for (IndexResponse response : responses) {
            ElasticsearchConfig.print(response);
        }
        client.close();


    }
}
