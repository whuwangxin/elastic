package elastic.learn.transport.index.onedocument.curd;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.transport.TransportClient;
import utils.LogUtil;

public class Get {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        GetResponse response = client.prepareGet("twitter", "tweet", "1").get();

        // Index name
        String index = response.getIndex();
        // Type name
        String type = response.getType();
        // Document ID (generated or not)
        String id = response.getId();
        // Version (if it's the first time you index this document, you will get: 1)
        long version = response.getVersion();


        LogUtil.i(index + "  " + type + "  " + id + "  " + version);

    }
}
