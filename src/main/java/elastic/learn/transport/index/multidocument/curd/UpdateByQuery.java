package elastic.learn.transport.index.multidocument.curd;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.UpdateByQueryAction;
import org.elasticsearch.index.reindex.UpdateByQueryRequestBuilder;

public class UpdateByQuery {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        UpdateByQueryRequestBuilder updateByQuery = UpdateByQueryAction.INSTANCE.newRequestBuilder(client);
        updateByQuery
                .source("index")
                .size(3)
                .abortOnVersionConflict(false);
        BulkByScrollResponse response = updateByQuery.get();
        System.out.println(response.getStatus());
    }
}
