package elastic.learn.transport.index.multidocument.curd;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.ReindexAction;

public class Reindex {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        BulkByScrollResponse response = ReindexAction.INSTANCE.newRequestBuilder(client)
                .source("index")
                .destination("new_index")
                .filter(QueryBuilders.matchQuery("gender", "male"))
                .get();
        System.out.println(response.getStatus());
    }
}
