package elastic.learn.transport.index.multidocument.curd;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequestBuilder;

import java.util.HashMap;
import java.util.Map;

public class SearchTemplate {
    public static void main(String[] args) {
        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        Map<String, Object> template_params = new HashMap<>();
        template_params.put("param_gender", "male");
        String json = "{\n" +
                "        \"query\" : {\n" +
                "            \"match\" : {\n" +
                "                \"gender\" : \"{{param_gender}}\"\n" +
                "            }\n" +
                "        }\n" +
                "}";
        SearchResponse sr = new SearchTemplateRequestBuilder(client)
                .setScript(json)
                .setScriptType(ScriptType.INLINE)
                .setScriptParams(template_params)
                .setRequest(new SearchRequest())
                .get()
                .getResponse();

        System.out.println(sr.toString());
    }
}
