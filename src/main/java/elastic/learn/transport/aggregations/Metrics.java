package elastic.learn.transport.aggregations;

import elastic.learn.transport.ElasticsearchConfig;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.avg.AvgAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.cardinality.CardinalityAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.geobounds.GeoBoundsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.max.MaxAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.min.MinAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.percentiles.PercentileRanksAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.percentiles.PercentilesAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStatsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.valuecount.ValueCountAggregationBuilder;
import org.elasticsearch.search.sort.SortOrder;

/**
 * @Author unclewang
 * @Date 2018/10/13 23:31
 */
public class Metrics {
    public static void main(String[] args) {


        TransportClient client = ElasticsearchConfig.getElasticsearchClient();
        SearchResponse sr = client.prepareSearch().setIndices("index")
                .addAggregation(min())
                .addAggregation(max())
                .addAggregation(sum())
                .addAggregation(avg())
                .addAggregation(stats())
                .addAggregation(extendedStats())
                .addAggregation(value())
                .addAggregation(percentiles())
                .addAggregation(percentilesValue())
                .addAggregation(percentileRanks())
                .addAggregation(cardinality())
                .addAggregation(top())
                .get();
//        Min min = sr.getAggregations().get("min");
//        Sum sum = sr.getAggregations().get("sum");
//
//        System.out.println(min);
//        System.out.println(sum);
        System.out.println(sr.getAggregations().getAsMap());
    }

    /**
     * 方法说明: 最小值
     */
    public static MinAggregationBuilder min() {
        MinAggregationBuilder aggregation =
                AggregationBuilders
                        .min("min")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回最大值
     */
    public static MaxAggregationBuilder max() {
        MaxAggregationBuilder aggregation =
                AggregationBuilders
                        .max("max")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回和
     */
    public static SumAggregationBuilder sum() {
        SumAggregationBuilder aggregation =
                AggregationBuilders
                        .sum("sum")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回平均数
     */
    public static AvgAggregationBuilder avg() {
        AvgAggregationBuilder aggregation =
                AggregationBuilders
                        .avg("avg")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回基本统计值
     */
    public static StatsAggregationBuilder stats() {
        StatsAggregationBuilder aggregation =
                AggregationBuilders
                        .stats("stats")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回更多的统计值，比如四分位数这种
     */
    public static ExtendedStatsAggregationBuilder extendedStats() {
        ExtendedStatsAggregationBuilder aggregation =
                AggregationBuilders
                        .extendedStats("extendedStats")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回有多少个文档有这个值
     */
    public static ValueCountAggregationBuilder value() {
        ValueCountAggregationBuilder aggregation =
                AggregationBuilders
                        .count("valueCount")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 以百分比返回
     */
    public static PercentilesAggregationBuilder percentiles() {
        PercentilesAggregationBuilder aggregation =
                AggregationBuilders
                        .percentiles("percentiles")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 以自定义的百分比返回
     */
    public static PercentilesAggregationBuilder percentilesValue() {
        PercentilesAggregationBuilder aggregation =
                AggregationBuilders
                        .percentiles("percentilesValue")
                        .field("age")
                        .percentiles(1.0, 5.0, 10.0, 20.0, 30.0, 75.0, 95.0, 99.0);
        return aggregation;
    }

    /**
     * 方法说明: 返回各种值所处在的比例
     */
    public static PercentileRanksAggregationBuilder percentileRanks() {
        PercentileRanksAggregationBuilder aggregation =
                AggregationBuilders
                        .percentileRanks("percentileRanks", new double[]{20, 30, 40})
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回基数，就是有多少个不同的值
     */
    public static CardinalityAggregationBuilder cardinality() {
        CardinalityAggregationBuilder aggregation =
                AggregationBuilders
                        .cardinality("cardinality")
                        .field("age");
        return aggregation;
    }

    /**
     * 方法说明: 返回地理信息
     */
    public static GeoBoundsAggregationBuilder geoBounds() {
        GeoBoundsAggregationBuilder aggregation =
                AggregationBuilders
                        .geoBounds("geoBounds")
                        .field("address.location")
                        .wrapLongitude(true);
        return aggregation;
    }

    /**
     * 方法说明: 返回各个聚合之后再按一定排序等规则进行聚合返回
     */
    public static AggregationBuilder top() {
        AggregationBuilder aggregation =
                AggregationBuilders
                        .terms("top").field("age")
                        .subAggregation(
                                AggregationBuilders.topHits("toptop")
                                        .sort("age", SortOrder.DESC)
//                                        .explain(true)
                                        .size(1)
//                                        .from(1)
                        );
        return aggregation;
    }
}
