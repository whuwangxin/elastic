package elastic.onestep.factory;


import elastic.onestep.config.Global;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.UnknownHostException;

/**
 * unclewang
 * 2018/5/25 14:12
 */
public class ClientFactory {

    private volatile static Client client;
    private  static final Settings settings = Settings.builder()
            .put("cluster.name", Global.getEsClusterName())
            //嗅探整个集群的状态，把能添加的机器添加进来
            .put("client.transport.sniff", true)
            // .put("xpack.security.user", "elastic:irlab&elastic")
            .build();

    public static Client get() {
        if (client == null) {
            synchronized (ClientFactory.class) {
                if (client == null) {
                    try {
                        client = new PreBuiltTransportClient(settings)
                                .addTransportAddresses(Global.getTransportAddresses());
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return client;
    }
}
