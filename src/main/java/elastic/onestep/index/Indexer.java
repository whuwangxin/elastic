package elastic.onestep.index;

public interface Indexer {
    void index();
}
