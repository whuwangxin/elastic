package elastic.onestep.bean;

import com.alibaba.fastjson.JSON;
import info.unclewang.DbUtilsDruid;
import org.apache.commons.dbutils.handlers.MapListHandler;

import java.io.*;
import java.util.List;
import java.util.Map;

public class Mysql2Json {
    public static void main(String[] args) throws IOException {
        DbUtilsDruid dbUtilsDruid = DbUtilsDruid.getInstance();
        String querySql = "SELECT * From evidence limit 7198";
        List<Map<String, Object>> mapList = (List<Map<String, Object>>) dbUtilsDruid.query(querySql, new MapListHandler());
        OutputStream stream = new FileOutputStream("/Users/unclewang/Idea_Projects/unclewang/src/main/resources/json/evidence.json");
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(stream));

        for (Map<String, Object> map : mapList) {
            Object jsonObject = JSON.toJSON(map);
            br.write(jsonObject.toString()+"\n");
            System.out.println(jsonObject.toString());
        }
        br.close();

    }
}
