package elastic.onestep.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class Visitor {
    private String id;
    private String ip;
    private Date visitDate;

    public Visitor() {
    }

    public Visitor(String id, String ip, Date visitDate) {
        this.id = id;
        this.ip = ip;
        this.visitDate = visitDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public static JSONObject randomVisit() {
        Visitor visitor = new Visitor();
        visitor.setId(UUID.randomUUID().toString());
        visitor.setIp(String.valueOf(new Random(1)));
        visitor.setVisitDate(new Date());
        JSONObject jsonObject= (JSONObject) JSON.toJSON(visitor);
        return jsonObject;

    }
}
