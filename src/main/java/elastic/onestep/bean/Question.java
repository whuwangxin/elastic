package elastic.onestep.bean;


/**
 * @Author unclewang
 * @Date 2018/5/23 16:03
 */

public class Question {

    private String qid;
    private String question;
    private int evidenceCount;


    public Question() {
    }

    public Question(String qid, String question, int evidenceCount) {
        this.qid = qid;
        this.question = question;
        this.evidenceCount = evidenceCount;

    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getEvidenceCount() {
        return evidenceCount;
    }

    public void setEvidenceCount(int evidenceCount) {
        this.evidenceCount = evidenceCount;
    }

}
