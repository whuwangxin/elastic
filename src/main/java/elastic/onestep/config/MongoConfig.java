package elastic.onestep.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MongoConfig {
    public static final String MONGO_PRROPERTIES = "mongo.properties";
    public static final String ADDRESS = "mongo.address";
    public static final String PORT = "mongo.port";
    private static Properties props = new Properties();

    static {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("mongo.properties");
        try {
            props.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getADDRESS() {
        return props.getProperty(ADDRESS);
    }

    public static Integer getPORT() {
        return Integer.valueOf(props.getProperty(PORT));
    }

}
